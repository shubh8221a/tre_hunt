import React, { useEffect, useState } from 'react'
import Pusher from 'pusher-js';
import './index.css'
const LeaderBoard = () => {
    const [leaderBoard,setLeaderBoard]=useState([]);
    useEffect(()=>{

        var pusher = new Pusher('e1e17be6a248886d499b', {
            cluster: 'ap2'
          });
        const channel = pusher.subscribe('leaderboard');
        channel.bind('answer',data=>{
            fetch(`${process.env.REACT_APP_BASE_URL}/leaderboard`)
        .then(res=>res.json())
        .then(data=>{
            setLeaderBoard(data.res)})
        .catch(err=>console.log(err))
        })
        fetch(`${process.env.REACT_APP_BASE_URL}/leaderboard`)
        .then(res=>res.json())
        .then(data=>{
            setLeaderBoard(data.res)})
        .catch(err=>console.log(err))
    },[])
  return (
    <div className="leaderboard" >
  <h1 className='leaderBoardHeading'> 
    LEADERBOARD
  </h1>
  <ol className='leaderBoardList'>
      <li>{leaderBoard[0]?.teamName}   {leaderBoard[0]?.questionSolved?`(${leaderBoard[0]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[1]?.teamName}   {leaderBoard[1]?.questionSolved?`(${leaderBoard[1]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[2]?.teamName}   {leaderBoard[2]?.questionSolved?`(${leaderBoard[2]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[3]?.teamName}   {leaderBoard[3]?.questionSolved?`(${leaderBoard[3]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[4]?.teamName}   {leaderBoard[4]?.questionSolved?`(${leaderBoard[4]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[5]?.teamName}   {leaderBoard[5]?.questionSolved?`(${leaderBoard[5]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[6]?.teamName}   {leaderBoard[6]?.questionSolved?`(${leaderBoard[6]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[7]?.teamName}   {leaderBoard[7]?.questionSolved?`(${leaderBoard[7]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[8]?.teamName}   {leaderBoard[8]?.questionSolved?`(${leaderBoard[8]?.questionSolved} Questions Solved)`:""}</li>
      <li>{leaderBoard[9]?.teamName}   {leaderBoard[9]?.questionSolved?`(${leaderBoard[9]?.questionSolved} Questions Solved)`:""}</li>
  </ol>
</div>
  )
}

export default LeaderBoard
