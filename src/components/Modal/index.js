import { motion } from "framer-motion";
import "./index.css";
import React from "react";
import Backdrop from "../Backdrop";

const Modal = ({ handleClose }) => {
  const dropIn = {
    hidden: {
      y: "-100vh",
      opacity: 0,
    },
    visible: {
      y: "0",
      opacity: 1,
      transition: {
        duration: 0.1,
        type: "spring",
        damping: 25,
        stiffness: 500,
      },
    },
    exit: {
      y: "100vh",
      opacity: 0,
    },
  };
  return (
    <Backdrop onClick={handleClose}>
      <motion.div
        onClick={(e) => e.stopPropagation()}
        className="modal"
        initial="hidden"
        animate="visible"
        exit="exit"
        variants={dropIn}
      >
        <h2 className="rules">Rules</h2>
        <div className="rule-container">
          <ol>
            <li style={{color:'black'}}>Use your Brain ! (And Maybe Google)</li>
            <li style={{color:'black'}}>We have 12 questions. You need to find the right answer and submit it to move to the next question. There are no limits for wrong answers.</li>
            <li style={{color:'black'}}>If the question is already answered by any of your teammates, you can ask them the answer for your curiosity and move ahead to the next question ! </li>
            <li style={{color:'black'}}>If you are stuck, there will be 1 hint given in the whole course of treasure hunt, so use it judiciously ! </li>
            <li style={{color:'black'}}>You have a total of 1 hour to find the treasure !</li>
            <li style={{color:'black'}}>You CANNOT skip any question. If you are stuck, jump to Rule no. 1</li>
            <li style={{color:'black'}}>PLEASE DO NOT PRESS REFRESH AT ANY POINT.</li>
          </ol>
        </div>
        <button onClick={handleClose} className="modal-close-btn">
          Ok
        </button>
      </motion.div>
    </Backdrop>
  );
};

export default Modal;
