export const QuizData = [
     {
        id: 0,
        question: 'When was RHEL 9 officially released ? (MM/DD/YYYY)',
        img: 'img5',
        options: [],
        type: 'WORD'
    },
    {
        id: 1,
        question: 'img3',
        options: [],
        type: 'IMG_Q'
    },
    {
        id: 2,
        question: `img2`,
        options: [],
        type: 'IMG_Q'
    },
    {
        id: 3,
        question: 'From which movie does this scene belong to ?',
        img: 'img10',
        options: [],
        type: 'WORD'
    },
    {
        id: 4,
        question: `img6`,
        options: [],
        type: 'IMG_Q'
    },
    {
        id: 5,
        question: `img1`,
        options: [],
        type: 'IMG_Q'
    },
    {
        id: 6,
        question: 'Which marvel character we see "turning to dust" first in Marvel Cinematic Universe ?',
        img: 'img4',
        options: [],
        type: 'WORD'
    },
    {
        id: 7,
        question: 'img7',
        options: [],
        type: 'IMG_Q'
    },
    {
        id: 8,
        question: 'Identify the person -',
        img: 'img9',
        options: [],
        type: 'WORD'
    },
    {
        id: 9,
        question: 'img8',
        downloadurl:"https://drive.google.com/file/d/1aXUue6BGtsvumQsEyEsNHTAeYq7r-fgz/view?usp=sharing",
        options: [],
        type: 'CODE'
    },
    {
        id: 10,
        question: 'Are you an "EXPERT" ? Try decoding this puzzle- ',
        downloadurl: "https://drive.google.com/file/d/1iGB4I47OAKHHILKvoQ3GJGbYMRPj-6ml/view?usp=sharing",
        img: 'img5',
        options: [],
        type: 'MUSIC'
    },
     {
        id: 11,
        question: 'imgkey',
        downloadurl:'https://drive.google.com/file/d/1ggXt6k4Ii0a1uD0qHAzUPr-Wj7PY37OM/view?usp=sharing',
        options: [],
        type: 'CODE'
    },
    {
        id: 12,
        type: 'OVER',
        question: 'Congratulations, you reached the end!'
    }

]


