import React, { Component } from "react";
import { QuizData } from "./QuizData";
import "./index.css";
import LeaderBoard from "../Leaderboard";


export class Quiz extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
      options: [],
      quizEnd: false,
      score: true,
      disabled: true,
      selectedOptionString: "",
      teamName: sessionStorage.getItem("teamName", 0),
      currentAnswer: "",
      attemptedQues: [0, 0, 0, 0, 0, 0]
    };


  }

  loadQuiz = () => {
    const { currentIndex } = this.state;
    this.setState(() => {
      return {
        question: QuizData[currentIndex].question,
        options: QuizData[currentIndex].options,
        answer: QuizData[currentIndex].answer,
      };
    });
  };

  nextQuestionHandler = () => {
    this.setState({
      currentIndex: this.state.currentIndex + 1,
      disabled: true,
    }, () => {
      this.render();
      try{
      document.getElementById(this.currentAnswer).value="";
      }
      catch(err) {
        console.log(err);
      };
    });


    // this.state.currentIndex = this.state.currentIndex + 1 ;
    // this.state.disabled = true;

  }

  componentDidMount() {
    this.loadQuiz();
  }

  //Responds to the click of the finish button
  finishHandler = () => {
    if (this.state.currentIndex === QuizData.length - 1) {
      this.setState({
        quizEnd: true,
      });
    }
  };

  selectHandler = () => {
    alert("check");
  };
  submitHandler = (answerid) => {
    if(this.state.teamName == undefined){
      if (window.confirm("You have not logged in ! Please login and try again !"))
{
window.location.href='http://128.168.133.126:3000/login';
}
else{
      alert("You have not logged in ! Please login and try again !")
}
    }
    else{
    const { answer } = this.state;
    const { score } = false;
    // console.log(answerid)
    try{
    var userAnswer = document.getElementById(this.currentAnswer).value;
    }
    catch(err){
    var userAnswer=""
    }    // console.log(this.state.attemptedQues[this.currentIndex])
    // console.log(this.state.attemptedQues)
    // if(this.state.attemptedQues[this.currentIndex]===1){

    //   alert("You have already attempted this question ! Go To the next question !")
    //       this.setState({
    //         disabled: false,
    //         score: true
    //       })

    // }
    // else{

    fetch(`${process.env.REACT_APP_BASE_URL}/submit`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        teamName: this.state.teamName,
        questionId: this.state.currentIndex,
        userAnswer: userAnswer
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        // console.log(response)



        if (response.status === 200 && response.msg === "Correct answer.Score Updated!!") {
          var aq = this.state.attemptedQues
          aq[this.state.currentIndex] = 1
          this.setState({
            disabled: false,
            score: true,
            attemptedQues: aq
          })
        }
        else if (response.status === 200 && response.msg === "Wrong answer!!") {
          alert("Incorrect answer !")
        }
        else if (response.status === 409) {
          alert("Your teammate has already solved this question ! Go To the next question !")
          this.setState({
            disabled: false,
            score: true
          })
        }
        else {

        }

      })
      .catch((err) => {
        // console.log(err);

      });


  };
};


  componentDidUpdate(prevProps, prevState) {
    const { currentIndex } = this.state;
    if (this.state.currentIndex !== prevState.currentIndex) {
      this.setState(() => {
        return {
          question: QuizData[currentIndex].question,
          options: QuizData[currentIndex].options,
          answer: QuizData[currentIndex].answer,
        };
      });
    }
  }

  render() {
  console.log(this.state.teamName)
  if(this.state.teamName=="ADMIN" | this.state.teamName=="admin"){
  console.log("enter if statement")
  return (
  <div>
            <div className="congrats">
            <h1 > Congratulations guys !</h1>
              <br></br>
            </div>
            <div>
             <LeaderBoard/>
             </div>
             </div>
          );
  }
  else{

  var d = new Date(); // for now
  console.log(d)
    //console.log(d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());
    const { question, options, currentIndex, userAnswer, quizEnd } = this.state;
    var item = currentIndex;
    for (let currentIndex in QuizData) {
      if (!quizEnd) {
        if (QuizData[item].type == "IMG_Q") {
          this.currentAnswer = "IMGanswer";
          return (
            <div className="quiz-container">
              <div class="container">
                <br></br>
                <h2>{QuizData[item].id + 1}. Solve this puzzle</h2>
                <br></br>
                <br></br>
                <div class={QuizData[item].question}></div>
                <div></div>
                <br></br>
                <br></br>
                <label for="answer">Your answer : </label>
                <input
                  type="text"
                  id="IMGanswer"
                  name="answer"
                  className="inputQuiz"
                />
                <br></br>
                <br></br>
                <button class="submitbutton" onClick={this.submitHandler.bind("IMGanswer")}>
                  Submit
                </button>

                <br></br>
                {currentIndex < QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.nextQuestionHandler}
                      disabled={this.state.disabled}
                    >
                      Next Question
                    </button>
                  )}
                <br></br>
                {currentIndex === QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.finishHandler}
                      disabled={this.state.disabled}
                    >
                      Finish
                    </button>
                  )}
                  <LeaderBoard/>
              </div>
            </div>
          );
        } else if (QuizData[item].type == "WORD") {
          this.currentAnswer = "WORDanswer";
          return (
            <div className="quiz-container">

              <div class="container">
              <br></br>
                <h2>
                  {QuizData[item].id + 1}. {QuizData[item].question}
                </h2>
                <br></br>
                <br></br>
                <div class={QuizData[item].img}>

                </div>
                <br></br>
                <label for="answer">Your answer : </label>
                <input
                  type="text"
                  id="WORDanswer"
                  name="answer"
                  className="inputQuiz"
                />
                <br></br>


                <button class="submitbutton" onClick={this.submitHandler.bind("WORDanswer")}>
                  Submit
                </button>

                <br></br>

                {currentIndex < QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.nextQuestionHandler}
                      disabled={this.state.disabled}
                    >
                      Next Question
                    </button>
                  )}
                <br></br>
                {currentIndex === QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.finishHandler}
                      disabled={this.state.disabled}
                    >
                      Finish
                    </button>
                  )}
                  <LeaderBoard/>
              </div>
            </div>
          );
        } else if (QuizData[item].type == "CODE") {
          this.currentAnswer = "CODEanswer";
           if(this.state.currentIndex==9){
           return (
            <div className="quiz-container">
              <div class="container">
              <br></br>
                <h2>
                  {QuizData[item].id + 1}. Decode the following-
                </h2>
                <button class="downloadbutton"><a href={QuizData[item].downloadurl} target="_blank">Download Link </a></button>
                <div className={QuizData[item].question}>
                </div>
                <br></br>
                <div className="divX">
                <label for="answer">Your answer : </label>
                <input
                  type="text"s
                  id="CODEanswer"
                  name="answer"
                  className="inputQuiz"
                />
                </div>
                <br></br>
                <br></br>
                <br></br>

                <button class="submitbutton" onClick={this.submitHandler.bind("CODEanswer")}>
                  Submit
                </button>

                <br></br>
                {currentIndex < QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.nextQuestionHandler}
                      disabled={this.state.disabled}
                    >
                      Next Question
                    </button>
                  )}
                <br></br>
                {currentIndex === QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.finishHandler}
                      disabled={this.state.disabled}
                    >
                      Finish
                    </button>
                  )}
                  <LeaderBoard/>
              </div>
            </div>
          );
           }
           else{return (
            <div className="quiz-container">
              <div class="container">
              <br></br>
                <h2>
                  {QuizData[item].id + 1}. Decode the following-
                </h2>
                <button class="downloadbutton"><a href={QuizData[item].downloadurl} target="_blank">Download Link </a></button>
                <div className={QuizData[item].question}>
                jHwIQb_Yzr3YPg_de84cKKi88d4XM8LJbhHuQooErWM=
                </div>
                <br></br>
                <div className="divX">
                <label for="answer">Your answer : </label>
                <input
                  type="text"s
                  id="CODEanswer"
                  name="answer"
                  className="inputQuiz"
                />
                </div>
                <br></br>
                <br></br>
                <br></br>

                <button class="submitbutton" onClick={this.submitHandler.bind("CODEanswer")}>
                  Submit
                </button>

                <br></br>
                {currentIndex < QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.nextQuestionHandler}
                      disabled={this.state.disabled}
                    >
                      Next Question
                    </button>
                  )}
                <br></br>
                {currentIndex === QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.finishHandler}
                      disabled={this.state.disabled}
                    >
                      Finish
                    </button>
                  )}
                  <LeaderBoard/>
              </div>
            </div>
          );}
        }  else if (QuizData[item].type == "MUSIC") {
          this.currentAnswer = "MUSICanswer";
          return (
            <div className="quiz-container">
              <div class="container">
                <br></br>

                <h2>
                  {QuizData[item].id + 1}. Are you an EXPERT ? Decode the following audio clip-
                </h2>
                <button class="downloadbutton"><a href={QuizData[item].downloadurl} target="_blank">Download Link </a></button>

                <br></br>

                <div className="divX">
                <label for="answer">Your answer : </label>
                <input
                  type="text"s
                  id="MUSICanswer"
                  name="answer"
                  className="inputQuiz"
                />
                </div>
                <br></br>
                <br></br>

                <button class="submitbutton" onClick={this.submitHandler.bind("MUSICanswer")}>
                  Submit
                </button>

                <br></br>
                {currentIndex < QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.nextQuestionHandler}
                      disabled={this.state.disabled}
                    >
                      Next Question
                    </button>
                  )}
                <br></br>
                {currentIndex === QuizData.length - 1 &&
                  this.state.score === true && (
                    <button
                      class="button"
                      onClick={this.finishHandler}
                      disabled={this.state.disabled}
                    >
                      Finish
                    </button>
                  )}
                  <LeaderBoard/>
              </div>
              
            </div>
          );
        }else if (QuizData[item].type == "OVER") {
          return (
            <div className="quizover-container">
              <h1 className="headingQuiz">{QuizData[item].question}</h1>

              <br></br>
            </div>
          );
        }
      }
    }
  }
  }
}

export default Quiz;
